const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: './test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'alice');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'bob');
    await page.keyboard.press('Enter', {delay: 400}); 
    //await page.waitForNavigation();

    await page.screenshot({ path: './test/screenshots/Welcome.png' });
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter', {delay: 300}); 
    //await page.waitForNavigation();

    await page.screenshot({ path:'./test/screenshots/Error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'alice');
    await page.waitFor(1000);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'bob');
    await page.waitFor(1000);
    await page.keyboard.press('Enter', {delay: 500}); 
    await page.waitFor(1000);
    let message = await page.$eval('body > div:nth-child(2) > ol > li > div:nth-child(2) > p',  (content) => content.innerHTML);
    await page.waitFor(1000);
    expect(message).toBe('Hi alice, Welcome to the chat app');

    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('R1');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Jolin', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('Jolin');
    
    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Jolin', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 

    let button = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });

    expect(button).toBe('Send');

    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Jolin', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 
    
    await page.type('#message-form > input[type=text]', 'yo', {delay:200});
    await page.keyboard.press('Enter', {delay: 200}); 

    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 
    
    await page.type('#message-form > input[type=text]', 'Hi', {delay:200});
    await page.keyboard.press('Enter', {delay: 200}); 
    
    await page.goto(url); 

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 
    
    await page.type('#message-form > input[type=text]', 'Hello', {delay:200});
    await page.keyboard.press('Enter', {delay: 200}); 

    await browser.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200}); 
    
    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');

    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    const context = browser.defaultBrowserContext();
    await context.overridePermissions(url, ['geolocation']);
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 200});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 200});
    await page.keyboard.press('Enter', {delay: 200});
    
    await page.click('button[id="send-location"]');
    await browser.close();
})
